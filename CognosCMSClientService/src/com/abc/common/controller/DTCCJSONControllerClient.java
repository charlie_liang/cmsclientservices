package com.abc.common.controller;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.abc.util.cache.CacheService;
import com.abc.util.cache.impl.DynaCacheServiceImpl;
import com.abc.common.model.MemberOverviewList;
import com.abc.common.service.CognosCMSService;
import com.abc.common.util.DataServiceConstants;
import com.abc.common.util.DataServiceUtil;
import com.abc.common.util.HTTPHelper;

@Controller
@RequestMapping("/mov")
public class abcJSONControllerClient {

    @Autowired
    @Qualifier("cognosCMSServiceImpl") 
	private CognosCMSService cmsService;

	@Autowired
	private HttpServletRequest request;
	
	@Resource(name="longLivedCache")
	private CacheService longLivedCache;
	private static final String CLASSNAME = abcJSONControllerClient.class.getName();
	private static Logger logger = Logger.getLogger(CLASSNAME);

	public CacheService getLongLivedCache() {
		return longLivedCache;
	}

	public void setLongLivedCache(CacheService longLivedCache) {
		this.longLivedCache = longLivedCache;
	}
	
	@RequestMapping(value = "/piechart", method = RequestMethod.GET)
	public @ResponseBody
	String getPieChartInJSON() {
		System.out.println("cacheService: " + longLivedCache);
		try {
			longLivedCache.put("getPieChartInJSON", "getPieChartInJSON");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		logger.entering(CLASSNAME, "getPieChartInJSON()");
		MemberOverviewList list = cmsService.getMemberOverviewPieChart(HTTPHelper.getLtpaToken(request));
		String json = "{}";
		if (list != null)
			json = DataServiceUtil.getPieChartJSONData(list.getRow());

		getHeadersInfo();
		logger.exiting(CLASSNAME, "getPieChartInJSON()");
		return json;
	}

	@RequestMapping(value = "/linechart", method = RequestMethod.GET)
	public @ResponseBody
	String getLineChartInJSON() {
		try {
			System.out.println("linecache: "+longLivedCache.get("getPieChartInJSON"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.entering(CLASSNAME, "getLineChartInJSON()");

		MemberOverviewList list = cmsService.getMemberOverviewLineChart(HTTPHelper.getLtpaToken(request));
		String json = "{}";
		if (list != null)
			json = DataServiceUtil.getLineChartJSONData(list.getRow());
		logger.exiting(CLASSNAME, "getLineChartInJSON()");
		return json;

	}

	@RequestMapping(value = "/viewgraph/linechart", params = {"dashboard", "participantids", "numofdays", "componentname"}, method = RequestMethod.GET)
	public @ResponseBody
	String getViewGraphLineChartInJSON(@RequestParam("dashboard") String dashboard, 
			@RequestParam("participantids") String participantids,
			@RequestParam("numofdays") String numofdays,
			@RequestParam("componentname") String componentname) {
		logger.entering(CLASSNAME, "getParticipantLineChartInJSON(). dashboard:"+dashboard+", participants:"+participantids+", " +
				"numofdays:"+numofdays +", componentname"+componentname);

		String params[] = {dashboard, participantids, numofdays, componentname};
		MemberOverviewList list = cmsService.getViewGraphLineChart(HTTPHelper.getLtpaToken(request), params);
		String json = "{}";
		if (list != null) 
			json = DataServiceUtil.getViewGrpahLineChartJSONData(list.getRow(), componentname);
		logger.exiting(CLASSNAME, "getParticipantLineChartInJSON()");
		return json;

	}

	@RequestMapping(value = "/myview", method = RequestMethod.GET)
	public @ResponseBody
	String getTablMyView() {
		logger.entering(CLASSNAME, "getTablMyView()");
		String analystid = request.getRemoteUser();
		if (analystid == null) analystid = "";
		String params[] = {"NSCC", analystid};
		MemberOverviewList list = cmsService.getMemberOverviewTable(DataServiceConstants.view_analyst, HTTPHelper.getLtpaToken(request), params);
		String json = "{}";
		if (list != null)
			json = DataServiceUtil.getMemberOverviewJSONData(list.getRow());

		logger.info("user: " + request.getRemoteUser());
		logger.exiting(CLASSNAME, "getTablMyView()");
		return json;
	}

	@RequestMapping(value = "/dashboard/{dept}", method = RequestMethod.GET)
	public @ResponseBody
	String getTableNSCCView(@PathVariable String dept) {
		logger.entering(CLASSNAME, "getTableNSCCView()");
		String params[] = {dept};
		MemberOverviewList list = cmsService.getMemberOverviewTable(DataServiceConstants.view_dept, HTTPHelper.getLtpaToken(request), params);
		String json = "{}";
		if (list != null)
			json = DataServiceUtil.getMemberOverviewJSONData(list.getRow());
		logger.exiting(CLASSNAME, "getTableNSCCView()");
		return json;
	}

	@RequestMapping(value = "/analystview/{analystid}", method = RequestMethod.GET)
	public @ResponseBody
	String getTableAnaLystView(@PathVariable String analystid) {
		logger.entering(CLASSNAME, "getTableAnaLystView()");
		String params[] = {"NSCC", analystid};
		MemberOverviewList list = cmsService.getMemberOverviewTable(DataServiceConstants.view_analyst, HTTPHelper.getLtpaToken(request), params);
		String json = "{}";
		if (list != null) 
			json = DataServiceUtil.getMemberOverviewJSONData(list.getRow());
		logger.exiting(CLASSNAME, "getTableAnaLystView()");
		return json;
	}

	@RequestMapping(value = "/analysts", method = RequestMethod.GET)
	public @ResponseBody
	List getAnaLystsInJSON() {
		logger.entering(CLASSNAME, "getAnaLysts()");
		MemberOverviewList list = cmsService.getAnalysts(HTTPHelper.getLtpaToken(request));
		logger.exiting(CLASSNAME, "getAnaLysts()");
		if (list == null)
			return null;
		else
			return list.getRow();
	}

	public CognosCMSService getCmsService() {
		return cmsService;
	}

	public void setCmsService(CognosCMSService cmsService) {
		this.cmsService = cmsService;
	}

	//get request headers
	private void getHeadersInfo() {

		Map<String, String> map = new HashMap<String, String>();

		Enumeration headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			map.put(key, value);
		}

		return;
	}

	@RequestMapping(value = "/dumpheaders", method = RequestMethod.GET)
	public @ResponseBody
	void getDumpHeaders() {
		System.out.println("getDumpHeaders()");
		getHeadersInfo();

		return;
	}
}