package com.abc.common.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.abc.common.model.MemberDetails;
import com.abc.common.model.MemberOverviewList;
import com.abc.common.service.CognosCMSService;
import com.abc.common.util.DataServiceConstants;
import com.abc.common.util.DataServiceUtil;
import com.abc.common.util.HTTPHelper;

@Controller
@RequestMapping("/details")
public class abcMemberDetailsController {

    @Autowired
    @Qualifier("cognosCMSServiceImpl")
	private CognosCMSService cmsService;
    
	@Autowired
	private HttpServletRequest request;
	
	@RequestMapping(value = "/data",  params = { "dashboard", "participantid"}, method = RequestMethod.GET)
	public @ResponseBody
	String getMemberDetails(@RequestParam("dashboard") String dashboard, 
			@RequestParam("participantid") String participantid) {
		String params[] = {dashboard, participantid};
		MemberOverviewList list = cmsService.getMemberDetails(DataServiceConstants.memberdetails, HTTPHelper.getLtpaToken(request), params);
		String md = null;
		if (list != null && list.getRow() != null) {
			 md =  DataServiceUtil.getMemberDetailsData(list.getRow());
		}
		
		return md;
	}
	
	@RequestMapping(value = "/linechart",  params = { "dashboard", "participantid", "numofdays"}, method = RequestMethod.GET)
	public @ResponseBody
	String getDetailsLinechart(@RequestParam("dashboard") String dashboard, 
			@RequestParam("participantid") String participantid,
			@RequestParam("numofdays") String numofdays) {
		String params[] = {dashboard, participantid, numofdays};
		MemberOverviewList list = cmsService.getMemberDetailsLinechart(DataServiceConstants.memberdetailslinechart, HTTPHelper.getLtpaToken(request), params);
		String md = null;
		if (list != null && list.getRow() != null) {
			 md =  DataServiceUtil.getLineChartJSONData(list.getRow());
		}
		
		return md;
	}
	
	@RequestMapping(value = "/piechart",  params = { "dashboard", "participantid"}, method = RequestMethod.GET)
	public @ResponseBody
	String getDetailsPiechart(@RequestParam("dashboard") String dashboard, 
			@RequestParam("participantid") String participantid) {
		String params[] = {dashboard, participantid, "1"};
		MemberOverviewList list = cmsService.getMemberDetailsLinechart(DataServiceConstants.memberdetailspiechart, HTTPHelper.getLtpaToken(request), params);
		String md = null;
		if (list != null && list.getRow() != null) {
			 md =  DataServiceUtil.getPieChartJSONData(list.getRow());
		}
		
		return md;
	}
	
	public CognosCMSService getCmsService() {
		return cmsService;
	}

	public void setCmsService(CognosCMSService cmsService) {
		this.cmsService = cmsService;
	}
}