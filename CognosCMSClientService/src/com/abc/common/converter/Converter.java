package com.abc.common.converter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.transform.stream.StreamSource;

import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.XmlMappingException;
import org.springframework.oxm.castor.CastorMarshaller;

import com.abc.common.model.DataSet;
import com.abc.common.util.DataServiceConstants;

public class Converter {

	private Marshaller marshaller;
	private Unmarshaller unmarshaller;
	private Unmarshaller unmarshallerLine;
	private Unmarshaller unmarshallerPie;
	private Unmarshaller unmarshallerAnalysts;
	private Unmarshaller unmarshallerDetails;
	private Unmarshaller unmarshallerViewGraphLinechart;

	public DataSet convertXML2Java(String xmlstring, String dataType) {
		InputStream is = null;
		Unmarshaller unmarsh = null;
		if (DataServiceConstants.linechart.equals(dataType) 
			||DataServiceConstants.memberdetailslinechart.equals(dataType))
			unmarsh = this.unmarshallerLine;
		else if (DataServiceConstants.viewgraph_linechart.equals(dataType) )
			unmarsh = this.unmarshallerViewGraphLinechart;
		else if (DataServiceConstants.piechart.equals(dataType) || DataServiceConstants.memberdetailspiechart.equals(dataType))
			unmarsh = this.unmarshallerPie;
		else if (DataServiceConstants.analysts.equals(dataType))
			unmarsh = this.unmarshallerAnalysts;
		else if (DataServiceConstants.memberdetails.equals(dataType))
			unmarsh = this.unmarshallerDetails;
		else
			unmarsh = this.unmarshaller;
		
		CastorMarshaller castor = (CastorMarshaller)unmarsh;
		castor.setIgnoreExtraElements(true);

		try {
			System.out.println("--- Unmarshaller ---");
			is = new ByteArrayInputStream(xmlstring.getBytes());
			DataSet ds = (DataSet) castor.unmarshal(new StreamSource(is));
			System.out.println("--- Unmarshaller Done! --- ");
			return ds;
		} catch (XmlMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	public void setMarshaller(Marshaller marshaller) {
		this.marshaller = marshaller;
	}

	public void setUnmarshaller(Unmarshaller unmarshaller) {
		this.unmarshaller = unmarshaller;
	}

	public Unmarshaller getUnmarshallerPie() {
		return unmarshallerPie;
	}

	public void setUnmarshallerPie(Unmarshaller unmarshallerPie) {
		this.unmarshallerPie = unmarshallerPie;
	}

	public Unmarshaller getUnmarshallerLine() {
		return unmarshallerLine;
	}

	public void setUnmarshallerLine(Unmarshaller unmarshallerLine) {
		this.unmarshallerLine = unmarshallerLine;
	}

	public Marshaller getMarshaller() {
		return marshaller;
	}

	public Unmarshaller getUnmarshaller() {
		return unmarshaller;
	}

	public Unmarshaller getUnmarshallerAnalysts() {
		return unmarshallerAnalysts;
	}

	public void setUnmarshallerAnalysts(Unmarshaller unmarshallerAnalysts) {
		this.unmarshallerAnalysts = unmarshallerAnalysts;
	}

	public Unmarshaller getUnmarshallerDetails() {
		return unmarshallerDetails;
	}

	public void setUnmarshallerDetails(Unmarshaller unmarshallerDetails) {
		this.unmarshallerDetails = unmarshallerDetails;
	}
	public Unmarshaller getUnmarshallerViewGraphLinechart() {
		return unmarshallerViewGraphLinechart;
	}
	public void setUnmarshallerViewGraphLinechart(
			Unmarshaller unmarshallerViewGraphLinechart) {
		this.unmarshallerViewGraphLinechart = unmarshallerViewGraphLinechart;
	}

}
