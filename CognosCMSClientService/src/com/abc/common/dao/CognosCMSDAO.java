package com.abc.common.dao;

public interface CognosCMSDAO {
	public String getCMSData(String dataType, String ltpa, String[] params);
}