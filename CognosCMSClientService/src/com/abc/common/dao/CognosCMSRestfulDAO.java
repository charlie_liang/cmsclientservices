package com.abc.common.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.abc.common.util.DataServiceConstants;
import com.abc.common.util.LTPAHeaderInterceptor;
import com.abc.common.util.ReadFile2String;

@Component
public class CognosCMSRestfulDAO implements CognosCMSDAO {
    
	@Value("${portal_base_url}")
	private String portalBaseUrl;
	
    @Autowired
    @Qualifier("LTPAHeaderInterceptor")
	private LTPAHeaderInterceptor LTPAInterceptor;
    
	@Value( "${CognosCMS.piechart_url}" )
	private String piechart_url;
	
	@Value( "${CognosCMS.linechart_url}" )
	private String linechart_url;
	
	@Value( "${CognosCMS.mov_dept_url}" )
	private String mov_dept_url;
	
	@Value( "${CognosCMS.mov_analyst_url}" )
	private String mov_analyst_url;
	
	@Value( "${CognosCMS.analyst_url}" )
	private String analyst_url;

	@Value( "${CognosCMS.details_url}" )
	private String details_url;
	
	@Value( "${CognosCMS.details_linechart_url}" )
	private String detailslinechart_url;
	
	private static final String CLASSNAME = CognosCMSRestfulDAO.class.getName();
	private static Logger logger = Logger.getLogger(CLASSNAME);
	
	@Override
	public String getCMSData(String dataType, String ltpa, String[] params) {
		String result = null;
		String endpoint = null;

		endpoint = getEndpoint(dataType, params);
		//if (dataType.equals(DataServiceConstants.piechart) || dataType.equals(DataServiceConstants.memberdetailspiechart) || dataType.equals(DataServiceConstants.memberdetailslinechart)) { 
		//	result =  getCMSDataFromFile(dataType); result=CleanupXML(result); System.out.println("result:"+result); return result;
		//}
		RestTemplate restTemplate = new RestTemplate();
        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
        interceptors.add(LTPAInterceptor);
        LTPAInterceptor.setLtpaToken(ltpa);
		restTemplate.setInterceptors(interceptors);
		restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
		
		// Make the HTTP GET request, marshaling the response to a String
		try {
			long timstamp = System.currentTimeMillis();
			result = restTemplate.getForObject(endpoint, String.class, "RESTful Cognos CMS call");
			timstamp = System.currentTimeMillis() - timstamp;
			logger.info("Cognos CMS call takes: " + timstamp/1000 + " (sec)");
		} catch (Exception e) {
			logger.severe("Restful call restTemplate.getForObject error: " + e.getMessage());
		}
		if (result==null)
			result="<?xml version=\"1.0\" ?><dataSet xmlns=\"http://www.ibm.com/xmlns/prod/cognos/dataSet/201006\" />";
		result = CleanupXML(result);

		return result;
	}
	
	private String CleanupXML(String result) {
		String str = result.replaceAll("&", "&amp;");
		return str;
	} 

	String getEndpoint(String dataType, String[] params) {
		String url = null;
		if (DataServiceConstants.linechart.equals(dataType)) {
			url = linechart_url;
		} else if  (DataServiceConstants.piechart.equals(dataType)) {
			url = piechart_url;
		} else if  (DataServiceConstants.analysts.equals(dataType)) {
			url = analyst_url;
		} else if  (DataServiceConstants.view_dept.equals(dataType)) {
			url = mov_dept_url;
			url = url.replaceAll("@param1", params[0]);
		} else if  (DataServiceConstants.view_analyst.equals(dataType)) {
			url = mov_analyst_url;
			url = url.replaceAll("@param1", params[0]);
			url = url.replaceAll("@param2", params[1]);
		} else if  (DataServiceConstants.memberdetails.equals(dataType)) {
			url = details_url;
			url = url.replaceAll("@param1", params[0]);
			url = url.replaceAll("@param2", params[1]);
			url = url.replaceAll("@param3", params[2]);
		} else if  (DataServiceConstants.memberdetailslinechart.equals(dataType)||DataServiceConstants.memberdetailspiechart.equals(dataType)) {
			url = detailslinechart_url;
			url = url.replaceAll("@param1", params[0]);
			url = url.replaceAll("@param2", params[1]);
			url = url.replaceAll("@param3", params[2]);
		}
		url = portalBaseUrl + url;
		logger.info("url - " +url);
		return url;
	}

	public String getPiechart_url() {
		return piechart_url;
	}

	public void setPiechart_url(String piechart_url) {
		this.piechart_url = piechart_url;
	}

	public String getLinechart_url() {
		return linechart_url;
	}

	public void setLinechart_url(String linechart_url) {
		this.linechart_url = linechart_url;
	}

	public String getMov_dept_url() {
		return mov_dept_url;
	}


	public void setMov_dept_url(String mov_dept_url) {
		this.mov_dept_url = mov_dept_url;
	}


	public String getMov_analyst_url() {
		return mov_analyst_url;
	}


	public void setMov_analyst_url(String mov_analyst_url) {
		this.mov_analyst_url = mov_analyst_url;
	}


	public String getAnalyst_url() {
		return analyst_url;
	}

	public void setAnalyst_url(String analyst_url) {
		this.analyst_url = analyst_url;
	}

	public String getDetails_url() {
		return details_url;
	}

	public void setDetails_url(String details_url) {
		this.details_url = details_url;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CognosCMSRestfulDAO dao = new CognosCMSRestfulDAO();
		String str = dao.getCMSData("Line", "ltpa", null);
	}
	
	private String getCMSDataFromFile(String dataType) {

		//local test data  
		String tmptbl = "piechart.xml";
		String tmpline = "linechart821.xml";
		String tmppie = "piechart821.xml";
		String tmpanalyst = "piechart.xml";
		String tmpdetails = "piechart.xml";
		//System.out.println("classpath: "+System.getProperty("java.class.path"));
		String str;
		if ("linechart".equals(dataType) || DataServiceConstants.participant_linechart.equals(dataType)
				||DataServiceConstants.memberdetailspiechart.equals(dataType)) {
			str = ReadFile2String.readFile(tmpline);
		} else if  ("piechart".equals(dataType)) {
			str = ReadFile2String.readFile(tmppie);
		} else if  ("analysts".equals(dataType)) {
			str = ReadFile2String.readFile(tmpanalyst);
		} else if  (DataServiceConstants.memberdetails.equals(dataType)) {
			str = ReadFile2String.readFile(tmpdetails);
		} else //table
		{
			str = ReadFile2String.readFile(tmptbl);
		}

		return str;
	}

	public String getPortalBaseUrl() {
		return portalBaseUrl;
	}

	public void setPortalBaseUrl(String portalBaseUrl) {
		this.portalBaseUrl = portalBaseUrl;
	}

	public String getDetailslinechart_url() {
		return detailslinechart_url;
	}

	public void setDetailslinechart_url(String detailslinechart_url) {
		this.detailslinechart_url = detailslinechart_url;
	}
}
