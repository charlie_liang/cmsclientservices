package com.abc.common.model;

import java.util.List;

public class DataSet {
	MemberOverviewList list;
	private String xmlns;

	public MemberOverviewList getList() {
		return list;
	}

	public void setList(MemberOverviewList list) {
		this.list = list;
	}

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}

}