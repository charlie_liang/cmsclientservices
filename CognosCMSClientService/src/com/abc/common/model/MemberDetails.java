package com.abc.common.model;

public class MemberDetails implements MemberOverview {
	private String participantId;
	private String participantName;
	private String date;
	private String timeslice;
	private float creditRating;
	private String status;
	private String analystId;
	private String enc;
	public String getEnc() {
		return enc;
	}
	public void setEnc(String enc) {
		this.enc = enc;
	}
	private float totalRequirement;
	private float totalRequirementPct;
	private String memberType;
	private float excessDeficit;
	private float excessDeficitPct;
	private float mtm;
	private float mtmPct;
	private float wimtm;
	private float wimtmPct;
	private float volatility;
	private float volatilityPct;
	private float cnsFails;
	private float cnsFailsPct;
	private float illiquid;
	private float illiquidPct;
	private float mmd;
	private float mmdPct;
	private float spc;
	private float spcPct;
	private float nonCNS;
	private float nonCNSPct;
	private float fundServ;
	private float fundServPct;
	private float fis;
	private float fisPct;
	private float mrd;
	private float mrdPct;
	private float coverage;
	private float coveragePct;
	private float special;
	private float specialPct;
	private float cfPremium;
	private float cfPremiumPct;
	
	public float getCreditRating() {
		return creditRating;
	}
	public void setCreditRating(float creditRating) {
		this.creditRating = creditRating;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public float getExcessDeficit() {
		return excessDeficit;
	}
	public void setExcessDeficit(float excessDeficit) {
		this.excessDeficit = excessDeficit;
	}
	public String getAnalystId() {
		return analystId;
	}
	public void setAnalystId(String analystId) {
		this.analystId = analystId;
	}
	public float getTotalRequirement() {
		return totalRequirement;
	}
	public void setTotalRequirement(float totalRequirement) {
		this.totalRequirement = totalRequirement;
	}
	public float getTotalRequirementPct() {
		return totalRequirementPct;
	}
	public void setTotalRequirementPct(float totalRequirementPct) {
		this.totalRequirementPct = totalRequirementPct;
	}
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public float getExcessDeficitPct() {
		return excessDeficitPct;
	}
	public void setExcessDeficitPct(float excessDeficitPct) {
		this.excessDeficitPct = excessDeficitPct;
	}
	public float getWimtm() {
		return wimtm;
	}
	public void setWimtm(float wimtm) {
		this.wimtm = wimtm;
	}
	public float getWimtmPct() {
		return wimtmPct;
	}
	public void setWimtmPct(float wimtmPct) {
		this.wimtmPct = wimtmPct;
	}
	public float getVolatility() {
		return volatility;
	}
	public void setVolatility(float volatility) {
		this.volatility = volatility;
	}
	public float getVolatilityPct() {
		return volatilityPct;
	}
	public void setVolatilityPct(float volatilityPct) {
		this.volatilityPct = volatilityPct;
	}
	public float getCnsFails() {
		return cnsFails;
	}
	public void setCnsFails(float cnsFails) {
		this.cnsFails = cnsFails;
	}
	public float getCnsFailsPct() {
		return cnsFailsPct;
	}
	public void setCnsFailsPct(float cnsFailsPct) {
		this.cnsFailsPct = cnsFailsPct;
	}
	public float getIlliquid() {
		return illiquid;
	}
	public void setIlliquid(float illiquid) {
		this.illiquid = illiquid;
	}
	public float getIlliquidPct() {
		return illiquidPct;
	}
	public void setIlliquidPct(float illiquidPct) {
		this.illiquidPct = illiquidPct;
	}
	public float getMmd() {
		return mmd;
	}
	public void setMmd(float mmd) {
		this.mmd = mmd;
	}
	public float getMmdPct() {
		return mmdPct;
	}
	public void setMmdPct(float mmdPct) {
		this.mmdPct = mmdPct;
	}
	public float getSpc() {
		return spc;
	}
	public void setSpc(float spc) {
		this.spc = spc;
	}
	public float getSpcPct() {
		return spcPct;
	}
	public void setSpcPct(float spcPct) {
		this.spcPct = spcPct;
	}
	public String getParticipantId() {
		return participantId;
	}
	public void setParticipantId(String participantId) {
		this.participantId = participantId;
	}
	public String getParticipantName() {
		return participantName;
	}
	public void setParticipantName(String participantName) {
		this.participantName = participantName;
	}
	public float getMtm() {
		return mtm;
	}
	public void setMtm(float mtm) {
		this.mtm = mtm;
	}
	public float getMtmPct() {
		return mtmPct;
	}
	public void setMtmPct(float mtmPct) {
		this.mtmPct = mtmPct;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTimeslice() {
		return timeslice;
	}
	public void setTimeslice(String timeslice) {
		this.timeslice = timeslice;
	}
	public float getNonCNS() {
		return nonCNS;
	}
	public void setNonCNS(float nonCNS) {
		this.nonCNS = nonCNS;
	}
	public float getNonCNSPct() {
		return nonCNSPct;
	}
	public void setNonCNSPct(float nonCNSPct) {
		this.nonCNSPct = nonCNSPct;
	}
	public float getFundServ() {
		return fundServ;
	}
	public void setFundServ(float fundServ) {
		this.fundServ = fundServ;
	}
	public float getFundServPct() {
		return fundServPct;
	}
	public void setFundServPct(float fundServPct) {
		this.fundServPct = fundServPct;
	}
	public float getFis() {
		return fis;
	}
	public void setFis(float fis) {
		this.fis = fis;
	}
	public float getFisPct() {
		return fisPct;
	}
	public void setFisPct(float fisPct) {
		this.fisPct = fisPct;
	}
	public float getMrd() {
		return mrd;
	}
	public void setMrd(float mrd) {
		this.mrd = mrd;
	}
	public float getMrdPct() {
		return mrdPct;
	}
	public void setMrdPct(float mrdPct) {
		this.mrdPct = mrdPct;
	}
	public float getCoverage() {
		return coverage;
	}
	public void setCoverage(float coverage) {
		this.coverage = coverage;
	}
	public float getCoveragePct() {
		return coveragePct;
	}
	public void setCoveragePct(float coveragePct) {
		this.coveragePct = coveragePct;
	}
	public float getSpecial() {
		return special;
	}
	public void setSpecial(float special) {
		this.special = special;
	}
	public float getSpecialPct() {
		return specialPct;
	}
	public void setSpecialPct(float specialPct) {
		this.specialPct = specialPct;
	}
	public float getCfPremium() {
		return cfPremium;
	}
	public void setCfPremium(float cfPremium) {
		this.cfPremium = cfPremium;
	}
	public float getCfPremiumPct() {
		return cfPremiumPct;
	}
	public void setCfPremiumPct(float cfPremiumPct) {
		this.cfPremiumPct = cfPremiumPct;
	}

}