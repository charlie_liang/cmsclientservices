package com.abc.common.model;

public class MemberOverviewAnalysts  implements MemberOverview {
	private String analystName;
	private String analystId;

	public String getAnalystName() {
		return analystId;
	}

	public void setAnalystName(String analystName) {
		this.analystName = analystName;
	}

	public String getAnalystId() {
		return analystId;
	}

	public void setAnalystId(String analystId) {
		this.analystId = analystId;
	}

}