package com.abc.common.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MemberOverviewLineChart  implements MemberOverview {
	
	private String clearCorpName;
	private String date;
	private String slicename;
	private float MTM;
	private float WIMTM;
	private float Volatility;
	private float CNSFails;
	private float illiquid;
	private float mmd;
	private float spc;
	private float Premium;
	private float Special;
	private float NonCNS;
	private float FundService;
	private float FIS;
	private float MRD;
	private float Coverage;
	
	private long timestamp;
	private float TotalRequirement;
	private float Deposit;
	private float ExcessDeficit;
	private float ENC;
	private String status;
	
	private static float max = 1000000;

	public void setTimestamp(long ts) {
		timestamp = ts;
	}
	public long getTimestamp() {
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
	    Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    Calendar c = Calendar.getInstance();
	    c.setTime(d);
	    long timestamp = c.getTimeInMillis();
		return timestamp;
	}
	public String getClearCorpName() {
		return clearCorpName;
	}
	public void setClearCorpName(String clearCorpName) {
		this.clearCorpName = clearCorpName;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getSlicename() {
		return slicename;
	}
	public void setSlicename(String slicename) {
		this.slicename = slicename;
	}
	public float getMTM() {
		return MTM/max;
	}
	public void setMTM(float mTM) {
		MTM = mTM;
	}
	public float getWIMTM() {
		return WIMTM/max;
	}
	public void setWIMTM(float wIMTM) {
		WIMTM = wIMTM;
	}
	public float getVolatility() {
		return Volatility/max;
	}
	public void setVolatility(float volatility) {
		Volatility = volatility;
	}
	public float getCNSFails() {
		return CNSFails/max;
	}
	public void setCNSFails(float cNSFails) {
		CNSFails = cNSFails;
	}
	public float getIlliquid() {
		return illiquid/max;
	}
	public void setIlliquid(float illiquid) {
		this.illiquid = illiquid;
	}
	public float getMmd() {
		return mmd/max;
	}
	public void setMmd(float mmd) {
		this.mmd = mmd;
	}
	public float getSpc() {
		return spc/max;
	}
	public void setSpc(float spc) {
		this.spc = spc;
	}
	public float getPremium() {
		return Premium/max;
	}
	public void setPremium(float premium) {
		Premium = premium;
	}
	public float getSpecial() {
		return Special/max;
	}
	public void setSpecial(float special) {
		Special = special;
	}
	public float getNonCNS() {
		return NonCNS/max;
	}
	public void setNonCNS(float nonCNS) {
		NonCNS = nonCNS;
	}
	public float getFundService() {
		return FundService/max;
	}
	public void setFundService(float fundService) {
		FundService = fundService;
	}
	public float getFIS() {
		return FIS/max;
	}
	public void setFIS(float fIS) {
		FIS = fIS;
	}
	public float getMRD() {
		return MRD/max;
	}
	public void setMRD(float mRD) {
		MRD = mRD;
	}
	public float getCoverage() {
		return Coverage/max;
	}
	public void setCoverage(float coverage) {
		Coverage = coverage;
	}
	public float getTotalRequirement() {
		return TotalRequirement/max;
	}
	public void setTotalRequirement(float totalRequirement) {
		TotalRequirement = totalRequirement;
	}
	public float getDeposit() {
		return Deposit/max;
	}
	public void setDeposit(float deposit) {
		Deposit = deposit;
	}
	public float getExcessDeficit() {
		return ExcessDeficit/max;
	}
	public void setExcessDeficit(float excessDeficit) {
		ExcessDeficit = excessDeficit;
	}
	public float getENC() {
		return ENC/max;
	}
	public void setENC(float eNC) {
		ENC = eNC;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

}