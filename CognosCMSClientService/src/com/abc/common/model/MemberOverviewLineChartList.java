package com.abc.common.model;

import java.util.List;

//refactor MemberOverview with generic & interface
public class MemberOverviewLineChartList implements MemberOverviewList {
	String id;
	List <MemberOverview> row;

	public List<MemberOverview> getRow() {
		return row;
	}

	public void setRow(List<MemberOverview> row) {
		this.row = row;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}