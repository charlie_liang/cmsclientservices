package com.abc.common.model;

import java.util.List;

//refactor MemberOverview with generic & interfaceMemberOverviewLineChartList
public interface MemberOverviewList {

	public abstract void setRow(List<MemberOverview> row);

	public abstract List<MemberOverview> getRow();
	
}