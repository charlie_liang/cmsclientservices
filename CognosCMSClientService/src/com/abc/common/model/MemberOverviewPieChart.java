package com.abc.common.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MemberOverviewPieChart  implements MemberOverview {
	private String clearCorpName;
	private String date;
	private float total;

	private float MTM;
	private float WIMTM;
	private float Volatility;
	private float CNSFails;
	private float illiquid;
	private float mmd;
	private float spc;
	private float Premium;
	private float Special;
	private float NonCNS;
	private float FundService;
	private float FIS;
	private float MRD;
	private float Coverage;
	
	private long timestamp;

	public long getTimestamp() {
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
	    Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    Calendar c = Calendar.getInstance();
	    c.setTime(d);
	    long timestamp = c.getTimeInMillis();
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public String getClearCorpName() {
		return clearCorpName;
	}
	public void setClearCorpName(String clearCorpName) {
		this.clearCorpName = clearCorpName;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public float getTotal() {
		return total;
	}
	public void setTotal(float total) {
		this.total = total;
	}
	public float getMTM() {
		return MTM;
	}
	public void setMTM(float mTM) {
		MTM = mTM;
	}
	public float getWIMTM() {
		return WIMTM;
	}
	public void setWIMTM(float wIMTM) {
		WIMTM = wIMTM;
	}
	public float getVolatility() {
		return Volatility;
	}
	public void setVolatility(float volatility) {
		Volatility = volatility;
	}
	public float getCNSFails() {
		return CNSFails;
	}
	public void setCNSFails(float cNSFails) {
		CNSFails = cNSFails;
	}
	public float getIlliquid() {
		return illiquid;
	}
	public void setIlliquid(float illiquid) {
		this.illiquid = illiquid;
	}
	public float getMmd() {
		return mmd;
	}
	public void setMmd(float mmd) {
		this.mmd = mmd;
	}
	public float getSpc() {
		return spc;
	}
	public void setSpc(float spc) {
		this.spc = spc;
	}
	public float getPremium() {
		return Premium;
	}
	public void setPremium(float premium) {
		Premium = premium;
	}
	public float getSpecial() {
		return Special;
	}
	public void setSpecial(float special) {
		Special = special;
	}
	public float getNonCNS() {
		return NonCNS;
	}
	public void setNonCNS(float nonCNS) {
		NonCNS = nonCNS;
	}
	public float getFundService() {
		return FundService;
	}
	public void setFundService(float fundService) {
		FundService = fundService;
	}
	public float getFIS() {
		return FIS;
	}
	public void setFIS(float fIS) {
		FIS = fIS;
	}
	public float getMRD() {
		return MRD;
	}
	public void setMRD(float mRD) {
		MRD = mRD;
	}
	public float getCoverage() {
		return Coverage;
	}
	public void setCoverage(float coverage) {
		Coverage = coverage;
	}

}