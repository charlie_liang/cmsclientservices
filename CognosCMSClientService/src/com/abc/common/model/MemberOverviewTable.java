package com.abc.common.model;


public class MemberOverviewTable implements MemberOverview {
	private String participantId;
	private String participantName;
	private float totalRequirement;
	private float totalRequirementPct;
	private float totalRequirementAvg;
	private float totalRequirementMax;
	private float excessDeficit;
	private float excessDeficitPct;
	private float excessDeficitAvg;
	private float excessDeficitMax;
	private float mtm;
	private float mtmPct;
	private float mtmAvg;
	private float mtmMax;
	private float wimtm;
	private float wimtmPct;
	private float wimtmAvg;
	private float wimtmMax;
	private float volatility;
	private float volatilityPct;
	private float volatilityAvg;
	private float volatilityMax;
	private float cnsFails;
	private float cnsFailsPct;
	private float cnsFailsAvg;
	private float cnsFailsMax;
	private float illiquid ;
	private float illiquidPct;
	private float illiquidAvg;
	private float illiquidMax;
	private float mmd;
	private float mmdPct;
	private float mmdAvg;
	private float mmdMax;
	private float spc;
	private float spcPct;
	private float spcAvg;
	private float spcMax;
	private String status;
	private String memType;

	private String date;
	private String timeslice;
	private String analyst;

	public String getParticipantId() {
		return participantId;
	}
	public void setParticipantId(String participantId) {
		this.participantId = participantId;
	}
	public String getParticipantName() {
		return participantName;
	}
	public void setParticipantName(String participantName) {
		this.participantName = participantName;
	}
	public float getTotalRequirement() {
		return totalRequirement;
	}
	public void setTotalRequirement(float totalRequirement) {
		this.totalRequirement = totalRequirement;
	}
	public float getExcessDeficit() {
		return excessDeficit;
	}
	public void setExcessDeficit(float excessDeficit) {
		this.excessDeficit = excessDeficit;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTimeslice() {
		return timeslice;
	}
	public void setTimeslice(String timeslice) {
		this.timeslice = timeslice;
	}
	public String getAnalyst() {
		return analyst;
	}
	public void setAnalyst(String analyst) {
		this.analyst = analyst;
	}
	public float getTotalRequirementPct() {
		return totalRequirementPct;
	}
	public void setTotalRequirementPct(float totalRequirementPct) {
		this.totalRequirementPct = totalRequirementPct;
	}
	public float getTotalRequirementAvg() {
		return totalRequirementAvg;
	}
	public void setTotalRequirementAvg(float totalRequirementAvg) {
		this.totalRequirementAvg = totalRequirementAvg;
	}
	public float getTotalRequirementMax() {
		return totalRequirementMax;
	}
	public void setTotalRequirementMax(float totalRequirementMax) {
		this.totalRequirementMax = totalRequirementMax;
	}
	public float getExcessDeficitPct() {
		return excessDeficitPct;
	}
	public void setExcessDeficitPct(float excessDeficitPct) {
		this.excessDeficitPct = excessDeficitPct;
	}
	public float getExcessDeficitAvg() {
		return excessDeficitAvg;
	}
	public void setExcessDeficitAvg(float excessDeficitAvg) {
		this.excessDeficitAvg = excessDeficitAvg;
	}
	public float getExcessDeficitMax() {
		return excessDeficitMax;
	}
	public void setExcessDeficitMax(float excessDeficitMax) {
		this.excessDeficitMax = excessDeficitMax;
	}
	public float getMtm() {
		return mtm;
	}
	public void setMtm(float mtm) {
		this.mtm = mtm;
	}
	public float getMtmPct() {
		return mtmPct;
	}
	public void setMtmPct(float mtmPct) {
		this.mtmPct = mtmPct;
	}
	public float getMtmAvg() {
		return mtmAvg;
	}
	public void setMtmAvg(float mtmAvg) {
		this.mtmAvg = mtmAvg;
	}
	public float getMtmMax() {
		return mtmMax;
	}
	public void setMtmMax(float mtmMax) {
		this.mtmMax = mtmMax;
	}

	public float getWimtm() {
		return wimtm;
	}
	public void setWimtm(float wimtm) {
		this.wimtm = wimtm;
	}
	public float getWimtmPct() {
		return wimtmPct;
	}
	public void setWimtmPct(float wimtmPct) {
		this.wimtmPct = wimtmPct;
	}
	public float getWimtmAvg() {
		return wimtmAvg;
	}
	public void setWimtmAvg(float wimtmAvg) {
		this.wimtmAvg = wimtmAvg;
	}
	public float getWimtmMax() {
		return wimtmMax;
	}
	public void setWimtmMax(float wimtmMax) {
		this.wimtmMax = wimtmMax;
	}
	public float getVolatility() {
		return volatility;
	}
	public void setVolatility(float volatility) {
		this.volatility = volatility;
	}
	public float getVolatilityPct() {
		return volatilityPct;
	}
	public void setVolatilityPct(float volatilityPct) {
		this.volatilityPct = volatilityPct;
	}
	public float getVolatilityAvg() {
		return volatilityAvg;
	}
	public void setVolatilityAvg(float volatilityAvg) {
		this.volatilityAvg = volatilityAvg;
	}
	public float getVolatilityMax() {
		return volatilityMax;
	}
	public void setVolatilityMax(float volatilityMax) {
		this.volatilityMax = volatilityMax;
	}
	public float getCnsFails() {
		return cnsFails;
	}
	public void setCnsFails(float cnsFails) {
		this.cnsFails = cnsFails;
	}
	public float getCnsFailsPct() {
		return cnsFailsPct;
	}
	public void setCnsFailsPct(float cnsFailsPct) {
		this.cnsFailsPct = cnsFailsPct;
	}
	public float getCnsFailsAvg() {
		return cnsFailsAvg;
	}
	public void setCnsFailsAvg(float cnsFailsAvg) {
		this.cnsFailsAvg = cnsFailsAvg;
	}
	public float getCnsFailsMax() {
		return cnsFailsMax;
	}
	public void setCnsFailsMax(float cnsFailsMax) {
		this.cnsFailsMax = cnsFailsMax;
	}
	public float getIlliquid() {
		return illiquid;
	}
	public void setIlliquid(float illiquid) {
		this.illiquid = illiquid;
	}
	public float getIlliquidPct() {
		return illiquidPct;
	}
	public void setIlliquidPct(float illiquidPct) {
		this.illiquidPct = illiquidPct;
	}
	public float getIlliquidAvg() {
		return illiquidAvg;
	}
	public void setIlliquidAvg(float illiquidAvg) {
		this.illiquidAvg = illiquidAvg;
	}
	public float getIlliquidMax() {
		return illiquidMax;
	}
	public void setIlliquidMax(float illiquidMax) {
		this.illiquidMax = illiquidMax;
	}
	public float getMmd() {
		return mmd;
	}
	public void setMmd(float mmd) {
		this.mmd = mmd;
	}
	public float getMmdPct() {
		return mmdPct;
	}
	public void setMmdPct(float mmdPct) {
		this.mmdPct = mmdPct;
	}
	public float getMmdAvg() {
		return mmdAvg;
	}
	public void setMmdAvg(float mmdAvg) {
		this.mmdAvg = mmdAvg;
	}
	public float getMmdMax() {
		return mmdMax;
	}
	public void setMmdMax(float mmdMax) {
		this.mmdMax = mmdMax;
	}
	public float getSpc() {
		return spc;
	}
	public void setSpc(float spc) {
		this.spc = spc;
	}
	public float getSpcPct() {
		return spcPct;
	}
	public void setSpcPct(float spcPct) {
		this.spcPct = spcPct;
	}
	public float getSpcAvg() {
		return spcAvg;
	}
	public void setSpcAvg(float spcAvg) {
		this.spcAvg = spcAvg;
	}
	public float getSpcMax() {
		return spcMax;
	}
	public void setSpcMax(float spcMax) {
		this.spcMax = spcMax;
	}
	public String getMemType() {
		return memType;
	}
	public void setMemType(String memType) {
		this.memType = memType;
	}

}