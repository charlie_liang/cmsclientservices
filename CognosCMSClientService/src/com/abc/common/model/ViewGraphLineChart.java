package com.abc.common.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ViewGraphLineChart  implements MemberOverview {
	
	private String participantid;
	private String participantname;
	private String date;
	private String timeslice;
	private long timestamp;
	private String memberType;
	private String status;
	private float dim8;
	private float dim9;
	private float dim10;
	private float dim11;
	private float dim12;
	private float dim13;
	private float dim14;
	private float dim15;
	private float dim16;
	private float dim17;
	private float dim18;
	private float dim19;
	private float dim20;
	private float dim21;
	private float dim22;
	private float dim23;
	private float dim24;
	private float dim25;

	public String getParticipantid() {
		return participantid;
	}
	public void setParticipantid(String participantid) {
		this.participantid = participantid;
	}
	public String getParticipantname() {
		return participantname;
	}
	public void setParticipantname(String participantname) {
		this.participantname = participantname;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTimeslice() {
		return timeslice;
	}
	public void setTimeslice(String timeslice) {
		this.timeslice = timeslice;
	}
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public void setTimestamp(long ts) {
		timestamp = ts;
	}
	public long getTimestamp() {
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
	    Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    Calendar c = Calendar.getInstance();
	    c.setTime(d);
	    long timestamp = c.getTimeInMillis();
		return timestamp;
	}
	public float getDim8() {
		return dim8;
	}
	public void setDim8(float dim8) {
		this.dim8 = dim8;
	}
	public float getDim9() {
		return dim9;
	}
	public void setDim9(float dim9) {
		this.dim9 = dim9;
	}
	public float getDim10() {
		return dim10;
	}
	public void setDim10(float dim10) {
		this.dim10 = dim10;
	}
	public float getDim11() {
		return dim11;
	}
	public void setDim11(float dim11) {
		this.dim11 = dim11;
	}
	public float getDim12() {
		return dim12;
	}
	public void setDim12(float dim12) {
		this.dim12 = dim12;
	}
	public float getDim13() {
		return dim13;
	}
	public void setDim13(float dim13) {
		this.dim13 = dim13;
	}
	public float getDim14() {
		return dim14;
	}
	public void setDim14(float dim14) {
		this.dim14 = dim14;
	}
	public float getDim15() {
		return dim15;
	}
	public void setDim15(float dim15) {
		this.dim15 = dim15;
	}
	public float getDim16() {
		return dim16;
	}
	public void setDim16(float dim16) {
		this.dim16 = dim16;
	}
	public float getDim17() {
		return dim17;
	}
	public void setDim17(float dim17) {
		this.dim17 = dim17;
	}
	public float getDim18() {
		return dim18;
	}
	public void setDim18(float dim18) {
		this.dim18 = dim18;
	}
	public float getDim19() {
		return dim19;
	}
	public void setDim19(float dim19) {
		this.dim19 = dim19;
	}
	public float getDim20() {
		return dim20;
	}
	public void setDim20(float dim20) {
		this.dim20 = dim20;
	}
	public float getDim21() {
		return dim21;
	}
	public void setDim21(float dim21) {
		this.dim21 = dim21;
	}
	public float getDim22() {
		return dim22;
	}
	public void setDim22(float dim22) {
		this.dim22 = dim22;
	}
	public float getDim23() {
		return dim23;
	}
	public void setDim23(float dim23) {
		this.dim23 = dim23;
	}
	public float getDim24() {
		return dim24;
	}
	public void setDim24(float dim24) {
		this.dim24 = dim24;
	}
	public float getDim25() {
		return dim25;
	}
	public void setDim25(float dim25) {
		this.dim25 = dim25;
	}

}