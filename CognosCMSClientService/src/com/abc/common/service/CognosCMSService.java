package com.abc.common.service;

import com.abc.common.model.MemberOverviewList;

public interface CognosCMSService {
	public MemberOverviewList getMemberOverviewPieChart(String ltpa);
	public MemberOverviewList getMemberOverviewLineChart(String ltpa);
	public MemberOverviewList getAnalysts(String ltpa);
	public MemberOverviewList getMemberOverviewTable(String dataType, String ltpaToken, String[] param1);
	public MemberOverviewList getViewGraphLineChart(String ltpa, String[] params);	
	public MemberOverviewList getMemberDetails(String dataType, String ltpaToken, String[] params);
	public MemberOverviewList getMemberDetailsLinechart(String dataType, String ltpaToken, String[] params);
	public MemberOverviewList getSettlement(String settlement, String ltpaToken, String[] params);
}
