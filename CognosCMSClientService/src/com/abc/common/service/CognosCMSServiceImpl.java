package com.abc.common.service;

import java.util.logging.Logger;

import javax.annotation.Resource;

import org.apache.soap.providers.com.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.abc.common.converter.Converter;
import com.abc.common.dao.CognosCMSRestfulDAO;
import com.abc.common.model.DataSet;
import com.abc.common.model.MemberOverviewList;
import com.abc.common.model.MemberOverviewTableList;
import com.abc.common.util.DataServiceConstants;

@Component
public class CognosCMSServiceImpl implements CognosCMSService {

    @Autowired
    @Qualifier("cognosCMSRestfulDAO")
	private CognosCMSRestfulDAO dao;
	
	@Resource(name="converterBean")
	Converter converter;
	
	private static final String CLASSNAME = CognosCMSServiceImpl.class.getName();
	private static Logger logger = Logger.getLogger(CLASSNAME);
	
	@Override
	public MemberOverviewList getMemberOverviewTable(String dataType, String ltpa, String[] params) {
		logger.entering(CLASSNAME, "getMemberOverviewTable(String ltpa)");
		String str = dao.getCMSData(dataType, ltpa, params);
		logger.info("MemberOverviewTable Cognos xml data:" + str);
		DataSet ds = converter.convertXML2Java(str, "mov_dept");
		System.out.println("ds: "+ds);
		MemberOverviewList list = null;
		if (ds != null) 
			list = ds.getList();
		else 
			list = new MemberOverviewTableList();
		
		logger.exiting(CLASSNAME, "getMemberOverviewTable(String ltpa)");
		return list;
	}
	
	private String getShortMsg(String str) {
		String tmp = null;
		int length = 200;
		if (str != null) {
			int len =  str.length();
			if (len > length) {
				tmp = str.substring(0, length);
			} else
				tmp = str;
		}
		return tmp;
	}

	@Override
	public MemberOverviewList getMemberOverviewPieChart(String ltpa) {
		String str = dao.getCMSData("piechart", ltpa, null);
		//logger.info("MemberOverviewPieChart Cognos xml data:" + str);
		DataSet ds = converter.convertXML2Java(str, "piechart");
		MemberOverviewList list = null;
		if (ds != null) 
			list = ds.getList();
		return list;
	}

	@Override
	public MemberOverviewList getMemberOverviewLineChart(String ltpa) {
		String str = dao.getCMSData("linechart", ltpa, null);
		//logger.info("MemberOverviewLineChart Cognos xml data:" + str);
		DataSet ds = converter.convertXML2Java(str, "linechart");
		MemberOverviewList list = null;
		if (ds != null) 
			list = ds.getList();
		return list;
	}
	
	@Override
	public MemberOverviewList getAnalysts(String ltpa) {
		String str = dao.getCMSData("analysts", ltpa, null);
		logger.info("Analysts Cognos xml data:" + str);
		DataSet ds = converter.convertXML2Java(str, "analysts");
		MemberOverviewList list = null;
		if (ds != null) 
			list = ds.getList();
		return list;
	}

	public Converter getConverter() {
		return converter;
	}

	public void setConverter(Converter converter) {
		this.converter = converter;
	}

	public CognosCMSRestfulDAO getDao() {
		return dao;
	}

	public void setDao(CognosCMSRestfulDAO dao) {
		this.dao = dao;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CognosCMSServiceImpl impl = new CognosCMSServiceImpl();
		//impl.getMemberOverviewLineChart();
		//impl.getMemberOverviewPieChart();
		//impl.getMemberOverviewTable();
	}

	@Override
	public MemberOverviewList getMemberDetails(String dataType, String ltpaToken, String[] params) {
		String str = dao.getCMSData(DataServiceConstants.memberdetails, ltpaToken, params);
		logger.info("MemberDetails Cognos xml data:" + str);
		DataSet ds = converter.convertXML2Java(str, DataServiceConstants.memberdetails);
		MemberOverviewList list = null;
		if (ds != null) 
			list = ds.getList();
		return list;
	}

	@Override
	public MemberOverviewList getViewGraphLineChart(String ltpa, String[] params) {
		String str = dao.getCMSData(DataServiceConstants.viewgraph_linechart, ltpa, params);
		//logger.info("ParticipantsLineChart Cognos xml data:" + str);
		DataSet ds = converter.convertXML2Java(str, DataServiceConstants.viewgraph_linechart);
		MemberOverviewList list = null;
		if (ds != null) 
			list = ds.getList();
		return list;
	}

	/**
	 * service class for member details line chart and pie chart. When it's pie chart, numofdays params[2]=1
	 */
	@Override
	public MemberOverviewList getMemberDetailsLinechart(String dataType, String ltpa, String[] params) {
		String str = dao.getCMSData(dataType, ltpa, params);
		//logger.info("getMemberDetailsLinechart Cognos xml data:" + str);
		DataSet ds = converter.convertXML2Java(str, dataType);
		MemberOverviewList list = null;
		if (ds != null) 
			list = ds.getList();
		return list;
	}

	@Override
	public MemberOverviewList getSettlement(String settlement,
			String ltpaToken, String[] params) {
		// TODO Auto-generated method stub
		return null;
	}
}
