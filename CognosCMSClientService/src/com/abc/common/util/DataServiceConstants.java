package com.abc.common.util;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

public final class DataServiceConstants {

	public static final String view_dept="view_dept";
	public static final String view_analyst="view_analyst";
	public static final String linechart="linechart";
	public static final String piechart="piechart";
	public static final String analysts="analysts";
	public static String memberdetails="memberdetails";
	public static String viewgraph_linechart="viewgraph_linechart";
	public static String memberdetailslinechart="memberdetailslinechart";
	public static String memberdetailspiechart="memberdetailspiechart";
	public static String settlement="settlement_overview";
	public static List<String> componentList=new ArrayList<String>() ;
	
	private DataServiceConstants() {}
	
	
	static {
		componentList.add("ENCEquity");
		componentList.add("TotalRequirement");
		componentList.add("ExcessDeficit");
		componentList.add("MTM");
		componentList.add("WIMTM");
		componentList.add("Volatility");
		componentList.add("CNSFails");
		componentList.add("Illiquid");
		componentList.add("MMD");
		componentList.add("SPC");
		componentList.add("CFPremium");
		componentList.add("Special");
		componentList.add("NonCNS");
		componentList.add("FundServCharge");
		componentList.add("FIS");
		componentList.add("MRD");
		componentList.add("Coverage");
		componentList.add("CreditRating");

	}
	
}
