package com.abc.common.util;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.Iterator;
import java.util.List;

import com.abc.common.model.MemberDetails;
import com.abc.common.model.MemberOverview;
import com.abc.common.model.MemberOverviewLineChart;
import com.abc.common.model.MemberOverviewPieChart;
import com.abc.common.model.MemberOverviewTable;
import com.abc.common.model.ViewGraphLineChart;

public class DataServiceUtil {
	public static String getPieChartJSONData(List <MemberOverview> row) {
	
		StringBuffer timestamp = new StringBuffer();	
		StringBuffer inner = new StringBuffer();
		StringBuffer otherStr = new StringBuffer();
		
		Iterator<MemberOverview> it = row.iterator();
		MemberOverviewPieChart mo = null;
		float otherVal = 0;
		if (it.hasNext()) {
			mo = (MemberOverviewPieChart)it.next();
			otherVal = otherVal + appendStr(inner, otherStr, "MTM", mo.getMTM());
			otherVal = otherVal + appendStr(inner, otherStr, "WI MTM", mo.getWIMTM()); 
			otherVal = otherVal + appendStr(inner, otherStr, "Volatility", mo.getVolatility());
			otherVal = otherVal + appendStr(inner, otherStr, "CNS Fails", mo.getCNSFails());
			otherVal = otherVal + appendStr(inner, otherStr, "Illiquid", mo.getIlliquid());
			otherVal = otherVal + appendStr(inner, otherStr, "MMD", mo.getMmd());
			otherVal = otherVal + appendStr(inner, otherStr, "SPC", mo.getSpc());
			otherVal = otherVal + appendStr(inner, otherStr, "CF Premium", mo.getPremium());
			otherVal = otherVal + appendStr(inner, otherStr, "Special", mo.getSpecial());
			otherVal = otherVal + appendStr(inner, otherStr, "Non-CNS", mo.getNonCNS());
			otherVal = otherVal + appendStr(inner, otherStr, "Fund/Serv", mo.getFundService());
			otherVal = otherVal + appendStr(inner, otherStr, "FIS", mo.getFIS());
			otherVal = otherVal + appendStr(inner, otherStr, "MRD", mo.getMRD());
			otherVal = otherVal + appendStr(inner, otherStr, "Coverage", mo.getCoverage());
			if (!otherStr.toString().isEmpty()) {
				inner.append(",[\"Other\",").append(getFloatPctStr(otherVal)).append("]");
			}
			
			timestamp.append(",\"Timestamp\":").append(mo.getTimestamp());		
		}
		
		StringBuffer sf = new StringBuffer();
		sf.append("[\n");
		sf.append(inner);
		sf.append("]");
		StringBuffer sfOther = new StringBuffer();
		sfOther.append("[\n");
		sfOther.append(otherStr);
		sfOther.append("]");
		StringBuffer json = new StringBuffer();
		json.append("{\"Data\":").append(sf).append(",\"Other\":").append(sfOther).append(timestamp).append("}");
		return json.toString();
	}

	private static float appendStr(StringBuffer inner, StringBuffer other, String name, float value) {
		float otherVal = 0;
		if (value < 0.05) {
			if (other.length() == 0) {
				other.append("[\"").append(name).append("\",").append(getFloatPctStr(value)).append("]");
			} else {
				other.append(",[\"").append(name).append("\",").append(getFloatPctStr(value)).append("]");
			}
			otherVal = value;
			value = 0;
		}

		if (inner.length() == 0) {
			inner.append("[\"").append(name).append("\",").append(getFloatPctStr(value)).append("]");
		} else {
			inner.append(",[\"").append(name).append("\",").append(getFloatPctStr(value)).append("]");
		}
		return otherVal;
	}

	/**
	 * keys: timestamp, ENC, MTM, clearCorpName, deposit, excessDeficit, totalRequirement, volatility
	 */
	public static String getLineChartJSONData(List <MemberOverview> row) {
		
		StringBuffer MTM = new StringBuffer();
		StringBuffer WIMTM = new StringBuffer();
		StringBuffer Volatility = new StringBuffer();
		StringBuffer CNSFails = new StringBuffer();
		StringBuffer Illiquid = new StringBuffer();
		StringBuffer MMD = new StringBuffer();
		StringBuffer SPC = new StringBuffer();
		StringBuffer Premium = new StringBuffer();
		StringBuffer Special = new StringBuffer();
		StringBuffer NonCNS = new StringBuffer();
		StringBuffer FundServ = new StringBuffer();
		StringBuffer FIS = new StringBuffer();
		StringBuffer MRD = new StringBuffer();
		StringBuffer Coverage = new StringBuffer();

		Iterator<MemberOverview> it = row.iterator();
		MemberOverviewLineChart mo = null;
		while (it.hasNext()) {
			mo = (MemberOverviewLineChart)it.next();
			MTM.append("[").append(mo.getTimestamp()).append(",").append(getFloatStr(mo.getMTM())).append("]");
			WIMTM.append("[").append(mo.getTimestamp()).append(",").append(getFloatStr(mo.getWIMTM())).append("]");
			Volatility.append("[").append(mo.getTimestamp()).append(",").append(getFloatStr(mo.getVolatility())).append("]");
			CNSFails.append("[").append(mo.getTimestamp()).append(",").append(getFloatStr(mo.getCNSFails())).append("]");
			Illiquid.append("[").append(mo.getTimestamp()).append(",").append(getFloatStr(mo.getIlliquid())).append("]");
			MMD.append("[").append(mo.getTimestamp()).append(",").append(getFloatStr(mo.getMmd())).append("]");
			SPC.append("[").append(mo.getTimestamp()).append(",").append(getFloatStr(mo.getSpc())).append("]");
			Premium.append("[").append(mo.getTimestamp()).append(",").append(getFloatStr(mo.getPremium())).append("]");
			Special.append("[").append(mo.getTimestamp()).append(",").append(getFloatStr(mo.getSpecial())).append("]");
			NonCNS.append("[").append(mo.getTimestamp()).append(",").append(getFloatStr(mo.getNonCNS())).append("]");
			FundServ.append("[").append(mo.getTimestamp()).append(",").append(getFloatStr(mo.getFundService())).append("]");
			FIS.append("[").append(mo.getTimestamp()).append(",").append(getFloatStr(mo.getFIS())).append("]");
			MRD.append("[").append(mo.getTimestamp()).append(",").append(getFloatStr(mo.getMRD())).append("]");
			Coverage.append("[").append(mo.getTimestamp()).append(",").append(getFloatStr(mo.getCoverage())).append("]");
			
			if (it.hasNext()) {
				MTM.append(",");
				WIMTM.append(",");
				Volatility.append(",");
				CNSFails.append(",");
				Illiquid.append(",");
				MMD.append(",");
				SPC.append(",");
				Premium.append(",");
				Special.append(",");
				NonCNS.append(",");
				FundServ.append(",");
				FIS.append(",");
				MRD.append(",");
				Coverage.append(",");
			}
		}
		
		StringBuffer sf = new StringBuffer();
		sf.append("{\n");
		sf.append("\"MTM\":[").append(MTM).append("],\n");
		sf.append("\"WI MTM\":[").append(WIMTM).append("],\n");
		sf.append("\"Volatility\":[").append(Volatility).append("],\n");
		sf.append("\"CNS Fails\":[").append(CNSFails).append("],\n");
		sf.append("\"Illiquid\":[").append(Illiquid).append("],\n");
		sf.append("\"MMD\":[").append(MMD).append("],\n");
		sf.append("\"SPC\":[").append(SPC).append("],\n");
		sf.append("\"CF Premium\":[").append(Premium).append("],\n");
		sf.append("\"Special\":[").append(Special).append("],\n");
		sf.append("\"Non-CNS\":[").append(NonCNS).append("],\n");
		sf.append("\"Fund/Serv\":[").append(FundServ).append("],\n");
		sf.append("\"FIS\":[").append(FIS).append("],\n");
		sf.append("\"MRD\":[").append(MRD).append("],\n");
		sf.append("\"Coverage\":[").append(Coverage).append("]\n");
		sf.append("}");
		return sf.toString();
	}

	public static String getMemberOverviewJSONData(List <MemberOverview> row) {
		System.out.println("getMemberOverviewJSONData");
		StringBuffer mbrs = new StringBuffer();
		mbrs.append("[");
		Iterator<MemberOverview> it = row.iterator();
		MemberOverviewTable mo = null;
		String timeslice = null;
		while (it.hasNext()) {
			System.out.println("hasNext");
			mo = (MemberOverviewTable)it.next();
			String member = getMemberOverviewJSON(mo);
			System.out.println("member: " + member);
			if (mbrs.length() > 1) {
				mbrs.append(",");
			}
			mbrs.append("{").append(member).append("}");
			if (timeslice == null) timeslice = getTimeSlice(mo);
		}
		mbrs.append("]");
		StringBuffer json = new StringBuffer();
		json.append("{");
		//if (timeslice != null) {
			json.append("\"timeSlice\":\"").append(timeslice).append("\",\"members\":").append(mbrs);
		//}
		json.append("}");
		System.out.println("json:"+json.toString());
		return json.toString();
	}

	private static String getTimeSlice(MemberOverviewTable bean) {
		String sl = null;
		try {
			BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());
			PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
			for (PropertyDescriptor pd : descriptors) {
				String name = pd.getName();
				if (name.equalsIgnoreCase("timeslice")) {
					Object value = pd.getReadMethod().invoke(bean, (Object[]) null);
					sl = (String)value;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sl;
	}

	private static String getMemberOverviewJSON(MemberOverviewTable bean) {
		StringBuffer sf = new StringBuffer();
		try {
			BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());
			PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
			for (PropertyDescriptor pd : descriptors) {
				String name = pd.getName();
				if (!name.equalsIgnoreCase("timeslice") && !name.equals("class")) {
					Object value = pd.getReadMethod().invoke(bean, (Object[]) null);
					if (value instanceof java.lang.Float) {
						sf.append(",\"").append(name).append("\":").append(getFloatStr((float)value));
					} else {
						if (sf.length() != 0) sf.append(",");
						sf.append("\"").append(name).append("\":").append("\"").append(value).append("\"");			
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sf.toString();
	}
	
	private static String getMemberDetailsJSON(MemberDetails bean) {
		StringBuffer sf = new StringBuffer();
		try {
			BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());
			PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
			for (PropertyDescriptor pd : descriptors) {
				String name = pd.getName();
				if (!name.equalsIgnoreCase("encEquity") && !name.equals("class")) {
					Object value = pd.getReadMethod().invoke(bean, (Object[]) null);
					if (value instanceof java.lang.Float) {
						sf.append(",\"").append(name).append("\":").append(getFloatStr((float)value));
					} else {
						if (sf.length() != 0) sf.append(",");
						sf.append("\"").append(name).append("\":").append("\"").append(value).append("\"");			
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		String json  = "{" + sf.toString() + "}";
		return json;
	}

	private static String getFloatStr(float flt) {
		StringBuffer buf = new StringBuffer();
	    java.util.Formatter formatter = new java.util.Formatter(buf);
		formatter.format("%.2f", flt);
		
		return buf.toString();
	}
	

	private static String getFloatPctStr(float flt) {
		StringBuffer buf = new StringBuffer();
	    java.util.Formatter formatter = new java.util.Formatter(buf);
		formatter.format("%.1f", flt*100);
		
		return buf.toString();
	}

	public static String getMemberDetailsData(List<MemberOverview> row) {
		Iterator<MemberOverview> it = row.iterator();
		MemberDetails mo = null;
		if (it.hasNext()) {
			mo = (MemberDetails)it.next();
		}

		return getMemberDetailsJSON(mo);
	}

	public static String getViewGrpahLineChartJSONData(List<MemberOverview> row, String compName) {
		Iterator<MemberOverview> it = row.iterator();
		ViewGraphLineChart mo = null;

		StringBuffer comp1 = new StringBuffer();

		while (it.hasNext()) {
			mo = (ViewGraphLineChart)it.next();
			comp1.append("[").append(mo.getTimestamp()).append(",").append(getFloatStr(getComponentValue(mo, compName))).append("]");
			if (it.hasNext()) {
				comp1.append(",");
			}
		}
		
		StringBuffer sf = new StringBuffer();
		sf.append("{\n");
		sf.append("\"").append(compName).append("\":[").append(comp1).append("],\n");
		sf.append("}");
		return sf.toString();
	}

	public static float getComponentValue(ViewGraphLineChart mo, String compName) {
		String fieldName = "dim" + getComponentIndex(compName);
		float ret = 0;
		try {
			BeanInfo beanInfo = Introspector.getBeanInfo(mo.getClass());
			PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
			for (PropertyDescriptor pd : descriptors) {
				String name = pd.getName();
				if (name.equals(fieldName)) {
					Object value = pd.getReadMethod().invoke(mo, (Object[]) null);
					if (value instanceof java.lang.Float) {
						ret = (Float)value;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	public static int getComponentIndex(String compName) {	
		int idx = DataServiceConstants.componentList.indexOf(compName);
		int dimIdx = idx+8;
		
		return dimIdx;
	}
	
	public static String getComponentSequence(String compName) {	
		int idx = DataServiceConstants.componentList.indexOf(compName);
		
		return Integer.toString(idx+1);
	}

}
