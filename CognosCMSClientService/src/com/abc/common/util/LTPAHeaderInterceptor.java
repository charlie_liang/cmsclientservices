package com.abc.common.util;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

@Component
public class LTPAHeaderInterceptor implements ClientHttpRequestInterceptor {

	private String ltpaToken;
    
    @Override
    public ClientHttpResponse intercept(
            HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException {

    	
    	//TODO: update to get LTPA
        HttpHeaders headers = request.getHeaders();
        System.out.println("LTPAHeaderInterceptor::ltpaToken: " + ltpaToken);
        String cookie = "LtpaToken2=" + ltpaToken;
        headers.add("Cookie", cookie);
        System.out.println("header added");
        return execution.execute(request, body);
    }

	public String getLtpaToken() {
		return ltpaToken;
	}

	public void setLtpaToken(String ltpaToken) {
		this.ltpaToken = ltpaToken;
	}
}