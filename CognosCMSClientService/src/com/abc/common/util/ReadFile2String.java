package com.abc.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.Scanner;

public class ReadFile2String {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String path = "C:/rad8/workspace3/RestWebApp/src/ATable.JSON";
		String file = readFile(path);
		System.out.println(file);
	}
	
	public static String readFile(String filename) {
         
         try {
        	 InputStream is = new ReadFile2String().getClass().getClassLoader().getResourceAsStream(filename);
        	 byte[] bytes = new byte[is.available()];
        	 is.read(bytes);
             return new String(bytes,"UTF-8");
         } catch (FileNotFoundException e) {
             e.printStackTrace();
         } catch (IOException e) {
             e.printStackTrace();
         }
         System.out.println("empty file:"+filename);
         return "";
	}

}
